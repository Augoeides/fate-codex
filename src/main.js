// general import
import Vue from 'vue'
import App from './App.vue'
import VueFire from 'vuefire'
import Firebase from 'firebase'
import VueRouter from 'vue-router'
import VueAnime from 'vue-animejs'
import VueAwesomeSwiper from 'vue-awesome-swiper'

//component import
import StartScreen from './components/start-screen'
import Wrapper from './components/wrapper'
import TopMenu from './components/top-menu'
import Home from './components/home'

import Info from './components/home/info'
import Soundtrack from './components/home/soundtrack'
import FullOst from './components/home/full-soundtrack'

import Story from './components/home/story'
import MainInfo from './components/home/main-info'
import Characters from './components/home/characters'
import ClassCat from './components/home/class-category'

import Gallery from './components/home/gallery'
import FullGallery from './components/home/full-gallery'
import Character from './components/home/character'
//end of import

/// init db
let config = {
    apiKey: "AIzaSyBv9TyfW7KSoPH6CyyFZJamLsSeiT6P6eU",
    authDomain: "fate-f8be1.firebaseapp.com",
    databaseURL: "https://fate-f8be1.firebaseio.com",
    projectId: "fate-f8be1",
    storageBucket: "fate-f8be1.appspot.com",
    messagingSenderId: "272148027475",
};
let app = Firebase.initializeApp(config);
let db = app.firestore();
db.settings({
    timestampsInSnapshots: true
});
/// end of init db
// global connection to db
Vue.use(VueFire);
Vue.prototype.$_db = db;
/// end of connection db

Vue.use(VueAwesomeSwiper);
Vue.prototype.$_swipe = VueAwesomeSwiper;


// router connection
Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    scrollBehavior: function (to, from, savedPosition) {
        if (savedPosition) {
            // savedPosition is only available for popstate navigations.
            return savedPosition
        } else {
            // scroll to anchor by returning the selector
            if (to.hash) {
                let elm = document.querySelector(to.hash).getBoundingClientRect().top;
                return window.scrollTo({
                    top: elm + window.scrollY,
                    behavior: 'smooth'
                });
            }

        }
    },
    routes: [
        {path: '/', name: '/', redirect: {name: 'welcome'}},

        {path: '/welcome', name: 'welcome', component: StartScreen},

        {
            path: '/', component: Wrapper,
            children: [
                {
                    path: '/home', components: {
                        default: Home,
                        menu: TopMenu,
                    },
                    children: [
                        {
                            path: '', name: 'home', components: {
                                info: Info,
                                gallery: Gallery,
                                ost: Soundtrack
                            }
                        },
                    ]
                },
                {path: 'story', name: 'story', component: Story},
                {
                    path: 'characters', name: 'characters', component: Characters, redirect: {name: 'class-cat', params: {class: 'rider'}},
                    children: [
                        {
                            path: ':class', name: 'class-cat', component: ClassCat,
                            children: [
                                {path: ':character', name: 'character', component: Character},
                            ]
                        }
                    ]
                },
                {path: 'info', name: 'main-info', component: MainInfo},
                {path: 'gallery', name: 'full-gallery', component: FullGallery},
                {path: 'ost', name: 'soundtrack', component: FullOst},
            ]
        }
    ]
});
// end of router connection

// global bus
let bus = new Vue();
Vue.prototype.$_bus = bus;
// bus

Vue.config.productionTip = true;

//anime init
Vue.use(VueAnime)
//anime init

//directive
//directive

new Vue({
    render: h => h(App),
    router: router,
    data: {},
    created() {
        let ths = this;
    }
}).$mount('#app');
